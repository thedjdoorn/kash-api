Welkom op de KASH-docs repo a.k.a. Project Hottentotus

KASH aka Project Bathyergidae
  * <strong> kash-api (Project Hottentotus): Een RESTful API voor de andere clients
  * [kash-docs (Project Glaber): Documentatie van de API en clients](https://gitlab.com/thedjdoorn/kash-docs)
  * [kash-web (Project Capensis): Een webinterface](https://gitlab.com/thedjdoorn/kash-web)
  * [kash-android (Project Suillus): Een Android client](https://gitlab.com/thedjdoorn/kash-android)
  * [kash-ios (Project Janetta): Een iOS client](https://gitlab.com/thedjdoorn/kash-ios)

  ___
  Op deze repo vind je documentatie in verschillende vormen, zoals
  * REST API Documentatie
  * Diagrammen
  * Usage Guides

  Enjoy!
